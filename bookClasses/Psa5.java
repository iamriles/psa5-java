class Psa5
{
  public static void main(String[] args) 
  {
    String picturesLoc = "Pictures\\";
     Picture pic1 = new Picture(picturesLoc + "luger.jpg");
     Picture pic2 = new Picture(picturesLoc + "goofball.jpg");
     Picture pic3 = new Picture(picturesLoc + "buu.png");
     
     //apply filters
     pic1.flipGreen();
     pic2.sunBurn();
     pic3.rotColors();
     
     Picture collage = createCollage(pic1, pic2, pic3);
     collage.write(picturesLoc + "LeRoy_PSA5.jpg");
  }
}