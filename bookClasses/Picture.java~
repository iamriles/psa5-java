import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.text.*;
import java.util.*;
import java.util.List; // resolves problem with java.awt.List and java.util.List

/**
 * A class that represents a picture.  This class inherits from 
 * SimplePicture and allows the student to add functionality to
 * the Picture class.  
 * 
 * Copyright Georgia Institute of Technology 2004-2005
 * @author Barbara Ericson ericson@cc.gatech.edu
 */
public class Picture extends SimplePicture 
{
  ///////////////////// constructors //////////////////////////////////
  
  /**
   * Constructor that takes no arguments 
   */
  public Picture ()
  {
    /* not needed but use it to show students the implicit call to super()
     * child constructors always call a parent constructor 
     */
    super();  
  }
  
  /**
   * Constructor that takes a file name and creates the picture 
   * @param fileName the name of the file to create the picture from
   */
  public Picture(String fileName)
  {
    // let the parent class handle this fileName
    super(fileName);
  }
  
  /**
   * Constructor that takes the width and height
   * @param width the width of the desired picture
   * @param height the height of the desired picture
   */
  public Picture(int width, int height)
  {
    // let the parent class handle this width and height
    super(width,height);
  }
  
  /**
   * Constructor that takes a picture and creates a 
   * copy of that picture
   */
  public Picture(Picture copyPicture)
  {
    // let the parent class do the copy
    super(copyPicture);
  }
  
  /**
   * Constructor that takes a buffered image
   * @param image the buffered image to use
   */
  public Picture(BufferedImage image)
  {
    super(image);
  }
  
  ////////////////////// methods ///////////////////////////////////////
  
  /**
   * Method to return a string with information about this picture.
   * @return a string with information about the picture such as fileName,
   * height and width.
   */
  public String toString()
  {
    String output = "Picture, filename " + getFileName() + 
      " height " + getHeight() 
      + " width " + getWidth();
    return output;
    
  }

  // small helper function for determining the maximum height.
  private static int maxHeightNeeded (Picture pic1,
                               Picture pic2,
                               Picture pic3)
  {
    Picture[] pics = {pic1, pic2, pic3};
    int max = 0;
    for (Picture pic : pics){
      if (pic.getHeight() > max) max = pic.getHeight();
    }
    return max;
  }
  
  // small helper function for determining the maximum width.
  private static int maxWidthNeeded (Picture pic1,
                              Picture pic2,
                              Picture pic3)
  {
    Picture[] pics = {pic1, pic2, pic3};
    int max = 0;
    for (Picture pic : pics){
      if (pic.getWidth() > max) max = pic.getWidth();
    }
    return max;
  }
  
  // simple ranged number generation
  private static int randInt(int min, int max) {
    Random rand = new Random();
    int randomNum = rand.nextInt((max - min) + 1) + min;
    return randomNum;
  }
  
  // used to "paste" an image given its starting position
  // should probably throw some kind of exception but what ever.
  private static void paste (Picture target, Picture source, int startx, int starty) {
    Pixel targetPixel = null;
    Pixel sourcePixel = null;
    //nested loop
    for (int targetx = startx,
         sourcex = 0; sourcex < source.getWidth();
         targetx++, sourcex++)
    {
      for (int targety = starty,
           sourcey = 0; sourcey < source.getHeight();
           targety++, sourcey++)
      {
        //bounds checking
        if (targetx < target.getWidth() && targety < target.getHeight()){
          sourcePixel = source.getPixel(sourcex, sourcey);
          targetPixel = target.getPixel(targetx, targety);
          targetPixel.setColor(sourcePixel.getColor());
        }
      }
    }
  }
  
  /**
   * This method takes 3 Pictures and will build and return a collage Picture
   * consisting of them.
   * @param pic1 component picture
   * @param pic2 component picture
   * @param pic3 component picture
   * @return a new Picture that is a collage o the 3 component pictures
   */
  public static Picture createCollage(Picture pic1,
                                      Picture pic2,
                                      Picture pic3)
  {
    //int maxW = maxWidthNeeded(pic1, pic2, pic3);
    //int maxH = maxHeightNeeded(pic1, pic2, pic3);
    int maxW = 800;
    int maxH = 800;
    
    Picture collage = new Picture(maxH, maxW);
    
    collage.paste(collage,
                  pic1,
                  0,
                  0);
    
    collage.paste(collage,
                  pic2,
                  randInt((int)(maxW * 0.1), (int)(maxW * 0.5)),
                  randInt((int)(maxH * 0.1), (int)(maxH * 0.5)));
    
    collage.paste(collage,
                  pic3,
                  randInt((int)(maxW * 0.1), (int)(maxW * 0.5)),
                  randInt((int)(maxH * 0.1), (int)(maxH * 0.5)));
    
    return collage;
  }
  
  //inceases red values after a certain red value
  public void sunBurn()
  {
    Pixel[] pixels = this.getPixels();
    int redVal = 0;
    for (Pixel pix : pixels)
    {
      redVal = pix.getRed();
      if (redVal > 150) redVal *= 1.5;
      pix.setRed(redVal);
    }
  }
  
  //rotates the color values
  public void rotColors()
  {
    Pixel[] pixels = this.getPixels();
    int temp = 0;
    for (Pixel pix : pixels){
      temp = pix.getBlue();
      pix.setGreen(pix.getBlue());
      pix.setBlue(pix.getRed());
      pix.setRed(temp);
    }
  }
  
  public void flipGreen()
  {
    Pixel[] pixels = this.getPixels();
    for (Pixel pix : pixels)
    {
          pix.setGreen(255 - pix.getGreen());
    }
  }
  
  public static void main(String[] args) 
  {
    String picturesLoc = "Pictures\\";
     Picture pic1 = new Picture(picturesLoc + "luger.jpg");
     Picture pic2 = new Picture(picturesLoc + "goofball.jpg");
     Picture pic3 = new Picture(picturesLoc + "buu.png");
     
     //apply filters
     pic1.flipGreen();
     pic2.sunBurn();
     pic3.rotColors();
     
     Picture collage = createCollage(pic1, pic2, pic3);
     collage.write(picturesLoc + "LeRoy_PSA5.jpg");
  }
  
} // this } is the end of class Picture, put all new methods before this
 